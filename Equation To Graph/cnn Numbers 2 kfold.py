#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 11:43:44 2018

@author: devjrmax
"""

from __future__ import print_function
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.preprocessing import image
import matplotlib.pyplot as plt
import numpy as np
import os
from tqdm import tqdm 
from skimage import color
import imageio
from sklearn.model_selection import KFold
from sklearn import metrics
from sklearn.utils import shuffle
from sklearn.metrics import roc_curve, auc
from scipy import interp
import matplotlib.pyplot as plt

batch_size = 64
epochs = 20

# input image dimensions
img_rows, img_cols = 28, 28

features_list = []
features = np.load("features numbers.npy")
features_label = np.load("features_label numbers.npy")

training_directory = "numbers3"

classes = os.listdir(training_directory)
classes.sort(key=lambda x: '{0:0>8}'.format(x).lower())

num_classes = len(classes)

x_data, y_data = shuffle(features, features_label, random_state=0)
features = []
features_label = []
y_data = keras.utils.to_categorical(y_data,num_classes)
kfold = KFold(n_splits = 5)
resultados = []
resultados2 = []
cmm =[]

fpr = dict()
tpr = dict()
roc_auc = dict()

input_shape = (img_rows, img_cols, 1)

tprs = []
aucs = []
mean_fpr = np.linspace(0, 1, 100)

fold = 0

for indice_treinamento, indice_teste in kfold.split(x_data):
    
    x_train = x_data[indice_treinamento].reshape(x_data[indice_treinamento].shape[0], img_rows, img_cols, 1)
    x_test = x_data[indice_teste].reshape(x_data[indice_teste].shape[0], img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')


    model = Sequential()
    model.add(Conv2D(32, kernel_size=3, activation='relu', input_shape=(28,28,1)))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(64, kernel_size=3, activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128, kernel_size=3, activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer='adam',
              metrics=['accuracy'])

    model.fit(x_train, y_data[indice_treinamento],
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test,y_data[indice_teste]))
    

    fold += 1
    
    score = model.evaluate(x_test,y_data[indice_teste], verbose=0)
    resultados.append(score[0])
    resultados2.append(score[1])
    predicao = model.predict(x_test)
    
    for i in range(num_classes):
        fpr[i], tpr[i], _ = roc_curve(y_data[indice_teste][:, i], predicao[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    fpr["micro"], tpr["micro"], _ = roc_curve(y_data[indice_teste].ravel(), predicao.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    
    y_pred = (predicao > 0.5)
    
    matrix = metrics.confusion_matrix(y_data[indice_teste].argmax(axis=1), y_pred.argmax(axis=1))
    print(matrix)
    cmm.append(matrix)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(num_classes)]))

    mean_tpr = np.zeros_like(all_fpr)
    for i in range(num_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])
    
    mean_tpr /= num_classes
    
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
    
    # Plot all ROC curves
    plt.figure()
    plt.plot(fpr["micro"], tpr["micro"],
             label='micro-average ROC curve (area = %f)' %(round(roc_auc["micro"],5)),
             color='deeppink', linestyle=':', linewidth=4)
    
    plt.plot(fpr["macro"], tpr["macro"],
             label='macro-average ROC curve (area = %f)' %(round(roc_auc["macro"],5)),
             color='navy', linestyle=':', linewidth=4)
    
    lw = 2
    
    
    
    for i in range(num_classes):
        plt.plot(fpr[i], tpr[i], lw=lw,
                 label='ROC of class %d (area = %f)' %(i, round(roc_auc[i],5)))
    
    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0, 0.2])
    plt.ylim([0.9, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC - CNN all classes of the number dataset - Fold %d' %(fold))
    plt.legend(loc="lower right")
    #plt.show()
    plt.savefig("cnnnumbers-fold"+str(fold)+".png")



'''
print(resultados)

soma = 0
cont = 0
for a in resultados:
    soma+=a
    cont+=1
media = soma/len(resultados)
print('Mean accuracy =', media)

testFiles = os.listdir("testing/")
testFiles.sort(key=lambda x: '{0:0>8}'.format(x).lower())

for i in testFiles:
    img = image.load_img(path="testing/"+i,grayscale=True,target_size=(28,28,1))
    img = image.img_to_array(img)
    test_img = img.reshape((1,28,28,1))
    img_class = model.predict_classes(test_img)
    prediction = img_class[0]
    classname = img_class[0]
    print("Class: ",classname)
    img = img.reshape((28,28))
    plt.imshow(img)
    plt.title(classname)
    plt.show()
'''