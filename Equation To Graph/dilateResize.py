#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 21:53:34 2018

@author: devjrmax
"""

import cv2
import numpy as np
import os
from tqdm import tqdm

training_directory = "numbers3"

def dilateResizeImage(name):

    im = cv2.imread(name)
    
    kernel = np.ones((3,3),np.uint8)

    im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    ret,thresh = cv2.threshold(im_gray,127,255,cv2.THRESH_BINARY_INV)

    dilated = cv2.dilate(thresh,kernel,iterations = 1)
    
    dilated = cv2.resize(dilated,(28,28))
        
    return dilated

classes = os.listdir(training_directory)
numClasses = len(classes)

for i in classes:
    directoryClass = training_directory+"/"+i
    for filename in tqdm(os.listdir(directoryClass)):
        fileFull = directoryClass+"/"+filename
        if (filename.endswith('.jpg')):
            dilated = dilateResizeImage(fileFull)
            cv2.imwrite(fileFull, dilated)