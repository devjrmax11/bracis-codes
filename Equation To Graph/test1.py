import numpy as np  
import matplotlib.pyplot as plt  
def graph(formula, x_range):  
    x = np.array(x_range)
    print(x)
    y = eval(formula)
    print(y)
    plt.plot(x, y)  
    plt.show()
    
graph('x**3+2*x-4', range(-10, 10))

formula = 'x**3+2*x-4'
x=5
y = eval(formula)
print(y)