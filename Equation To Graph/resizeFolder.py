#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 20:52:38 2018

@author: devjrmax
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 21:53:34 2018

@author: devjrmax
"""

import cv2
import numpy as np
import os
from tqdm import tqdm

training_directory = "symbols/3"

def resizeImage(name):

    im = cv2.imread(name)
    
    im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    im_gray = cv2.resize(im_gray,(50,50))
        
    return im_gray


for filename in tqdm(os.listdir(training_directory)):
    fileFull = training_directory+"/"+filename
    if (filename.endswith('.jpg')):
        dilated = resizeImage(fileFull)
        cv2.imwrite(fileFull, dilated)