#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 11:43:44 2018

@author: devjrmax
"""

from __future__ import print_function
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.preprocessing import image
import matplotlib.pyplot as plt
import numpy as np
import os
from tqdm import tqdm 
from skimage import color
import imageio
from sklearn.model_selection import KFold
from sklearn import metrics
from sklearn.utils import shuffle
from sklearn.metrics import roc_curve, auc
from scipy import interp
import matplotlib.pyplot as plt

batch_size = 64
epochs = 20

# input image dimensions
img_rows, img_cols = 28, 28

features_list = []
features = np.load("features.npy")
features_label = np.load("features_label.npy")

training_directory = "numsym"

classes = os.listdir(training_directory)
classes.sort(key=lambda x: '{0:0>8}'.format(x).lower())

num_classes = len(classes)

x_data, y_data = shuffle(features, features_label, random_state=0)
features = []
features_label = []
y_data = keras.utils.to_categorical(y_data,num_classes)
kfold = KFold(n_splits = 5)
resultados = []
resultados2 = []
cmm =[]


input_shape = (img_rows, img_cols, 1)

tprs = []
aucs = []
mean_fpr = np.linspace(0, 1, 100)

fold = 0

for indice_treinamento, indice_teste in kfold.split(x_data):
    
    x_train = x_data[indice_treinamento].reshape(x_data[indice_treinamento].shape[0], img_rows, img_cols, 1)
    x_test = x_data[indice_teste].reshape(x_data[indice_teste].shape[0], img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')


    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))

#model.compile(loss=keras.losses.categorical_crossentropy,
#              optimizer=keras.optimizers.Adadelta(),
#              metrics=['accuracy'])
    model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer='adam',
              metrics=['accuracy'])

    model.fit(x_train, y_data[indice_treinamento],
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test,y_data[indice_teste]))
    

    fold += 1
    
    score = model.evaluate(x_test,y_data[indice_teste], verbose=0)
    resultados.append(score[0])
    resultados2.append(score[1])
    predicao = model.predict(x_test)
    
    y_pred = (predicao > 0.5)
    
    fpr, tpr, thresholds = roc_curve(y_data[indice_teste].argmax(axis=1), predicao[:, 1])
    tprs.append(interp(mean_fpr, fpr, tpr))
    tprs[-1][0] = 0.0
    roc_auc = auc(fpr, tpr)
    aucs.append(roc_auc)
    plt.plot(fpr, tpr, lw=1, alpha=0.3, label='ROC fold %d (AUC = %f)' % (i, round(roc_auc,5)))
    matrix = metrics.confusion_matrix(y_data[indice_teste].argmax(axis=1), y_pred.argmax(axis=1))
    print(matrix)
    cmm.append(matrix)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    
plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
         label='Chance', alpha=.8)

mean_tpr = np.mean(tprs, axis=0)
mean_tpr[-1] = 1.0
mean_auc = auc(mean_fpr, mean_tpr)
std_auc = np.std(aucs)
plt.plot(mean_fpr, mean_tpr, color='b',
         label=r'Mean ROC (AUC = %f $\pm$ %f)' % (round(mean_auc,5), round(std_auc,5)),
         lw=2, alpha=.8)

std_tpr = np.std(tprs, axis=0)
tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                 label=r'$\pm$ 1 std. dev.')

plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC - CNN all classes of the number/symbol dataset - Fold %d' %(fold))
plt.legend(loc="lower right")
plt.show()


print(resultados)

soma = 0
cont = 0
for a in resultados:
    soma+=a
    cont+=1
media = soma/len(resultados)
print('Mean accuracy =', media)

symbol =['number', 'symbol']

testFiles = os.listdir("testing/")
testFiles.sort(key=lambda x: '{0:0>8}'.format(x).lower())

for i in testFiles:
    img = image.load_img(path="testing/"+i,grayscale=True,target_size=(28,28,1))
    img = image.img_to_array(img)
    test_img = img.reshape((1,28,28,1))
    img_class = model.predict_classes(test_img)
    prediction = img_class[0]
    classname = img_class[0]
    print("Class: ", symbol[classname])
    img = img.reshape((28,28))
    plt.imshow(img)
    plt.title(symbol[classname])
    plt.show()