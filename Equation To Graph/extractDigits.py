import cv2
import numpy as np
from PIL import Image
import statistics

def saveThresh(name):
    im = cv2.imread(name)

    im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    ret,thresh = cv2.threshold(im_gray,127,255,cv2.THRESH_BINARY_INV)
    
    cv2.imwrite(name, thresh)

def sizeCenter(name):
    im = Image.open(name)
    width, height = im.size
    if(width == height):
        size = width
    elif(width>height):
        size = width
    elif(height>width):
        size = height
    til = Image.new("RGB",(size,size), "white")
    x = int(size/2 - width/2)
    y = int(size/2 - height/2)
    til.paste(im,(x,y))
    til = til.resize((28, 28), Image.ANTIALIAS)
    til.save(name)

def sort_contours(cnts, method="left-to-right"):
	reverse = False
	i = 0
 
	if method == "right-to-left" or method == "bottom-to-top":
		reverse = True
 
	if method == "top-to-bottom" or method == "bottom-to-top":
		i = 1
 
	boundingBoxes = [cv2.boundingRect(c) for c in cnts]
	(cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
		key=lambda b:b[1][i], reverse=reverse))
 
	return (cnts)



idx = 0

im = cv2.imread("testjr123.jpg")

im = cv2.medianBlur(im,1)

im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

ret,thresh = cv2.threshold(im_gray,127,255,cv2.THRESH_BINARY_INV)

_ , contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print("no of shapes {0}".format(len(contours)))
height = thresh.shape[0]
print(height)
meio = int(height/2)
print(meio)

sortedContours =  sort_contours(contours)
allCy = []
idx = 0

for cnt in sortedContours:     
    x,y,w,h = cv2.boundingRect(cnt)
    cy = y +(h/2)
    allCy.append(cy)  
stdDev = statistics.stdev(allCy)
#print(stdDev)

for cnt in sortedContours:       
    x,y,w,h = cv2.boundingRect(cnt)
    M = cv2.moments(cnt)
    print(x,y,w,h) 
    #cy = int(M['m01']/M['m00'])
    cy = y +(h/2)
    
    roi = im[y:y + h, x:x + w]
    
    
    print(str(idx)+"="+str(cy))
    
    if((cy+stdDev)<meio):
        typeRec = "exp"
    else:
        typeRec = "base"
    print(typeRec)
    cv2.imwrite('testing/' + str(idx) +'-'+typeRec+'.jpg', roi)
    sizeCenter('testing/' + str(idx) +'-'+typeRec+'.jpg')
    saveThresh('testing/' + str(idx) +'-'+typeRec+'.jpg');
    
    idx += 1

