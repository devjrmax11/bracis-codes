from __future__ import print_function
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
import numpy as np
import os
from tqdm import tqdm 
from skimage import color
import imageio
from sklearn.model_selection import  train_test_split

batch_size = 128
num_classes = 21
epochs = 20

# input image dimensions
img_rows, img_cols = 50, 50

features_list = []
features_label = []

training_directory = "mathTraining"

classes = os.listdir(training_directory)
classes.sort(key=lambda x: '{0:0>8}'.format(x).lower())

numClasses = len(classes)

for i in range(numClasses):
    directoryClass = training_directory+"/"+str(i)
    for filename in tqdm(os.listdir(directoryClass)):
        if (filename.endswith('.jpg')):
            fileFull = directoryClass+"/"+filename
            training_digit_image = imageio.imread(fileFull)
            training_digit_image = color.rgb2gray(training_digit_image)
            features_list.append(training_digit_image)
            features_label.append(i)

features  = np.array(features_list, 'float64')

x_train, x_test, y_train, y_test = train_test_split(features, features_label)

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train2 = y_train
y_test2 = y_test
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

model.save('model.h5')