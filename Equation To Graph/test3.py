#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 17:39:45 2018

@author: devjrmax
"""

from skimage.feature import hog
from skimage import color,transform
import imageio
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
import cv2


knn = joblib.load('knn_model_numbers_symbol2.pkl')

def feature_extraction(image):
    return hog(image, orientations=8, pixels_per_cell=(10, 10), cells_per_block=(5, 5))

def predict(df):
    predict = knn.predict(df.reshape(1,-1))[0]
    predict_proba = knn.predict_proba(df.reshape(1,-1))
    return predict, predict_proba[0][predict]

digits = []

image = imageio.imread("testing/8.jpg")
image = cv2.resize(image, (50, 50), cv2.INTER_AREA)
image = color.rgb2gray(image)
digits.append(image)

# extract features
hogs = list(map(lambda x: feature_extraction(x), digits))

# apply k-NN model created in previous
predictions = list(map(lambda x: predict(x), hogs))
print(predictions)