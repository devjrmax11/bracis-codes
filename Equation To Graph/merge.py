#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 21:53:34 2018

@author: devjrmax
"""

import cv2
import numpy as np
import os
from tqdm import tqdm

training_directory = "numbers3"
save_directory = "numsym/0/"


classes = os.listdir(training_directory)
numClasses = len(classes)
num = 0
for i in classes:
    directoryClass = training_directory+"/"+i
    for filename in tqdm(os.listdir(directoryClass)):
        fileFull = directoryClass+"/"+filename
        if (filename.endswith('.jpg')):
            im = cv2.imread(fileFull)
            cv2.imwrite(save_directory+str(num)+".jpg", im)
            num=num+1