import numpy as np
import os
from tqdm import tqdm 
from skimage.feature import hog
from skimage import color
import imageio
from sklearn.model_selection import  train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib

def feature_extraction(image):
    image = color.rgb2gray(image)
    return hog(image, orientations=8, pixels_per_cell=(10, 10), cells_per_block=(5, 5))

def predict(df):
    predict = knn.predict(df.reshape(1,-1))[0]
    predict_proba = knn.predict_proba(df.reshape(1,-1))
    return predict, predict_proba[0][predict]

features_list = []
features_label = []

training_directory = "mathTraining"

classes = os.listdir(training_directory)
numClasses = len(classes)

for i in range(numClasses):
    directoryClass = training_directory+"/"+str(i)
    for filename in tqdm(os.listdir(directoryClass)):
        if (filename.endswith('.jpg')):
            fileFull = directoryClass+"/"+filename
            training_digit_image = imageio.imread(fileFull)
            df= feature_extraction(training_digit_image)            
       
            features_list.append(df)
            features_label.append(i)

features  = np.array(features_list, 'float64')

X_train, X_test, y_train, y_test = train_test_split(features, features_label)

knn = KNeighborsClassifier(n_neighbors=3)

knn.fit(X_train, y_train)

model_score = knn.score(X_test, y_test)

joblib.dump(knn, 'knn_model_numbers_symbol2.pkl')
