from skimage import color
import imageio
from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os

names = ['0','1','2', '3', '4', '5', '6', '7', '8', '9', '=', '-', '(', ')', '[',']','{', '}', '+','div','x']
types=['number', 'symbol']

# load the model we saved
model = load_model('modelNumSym.h5')
model2 = load_model('modelNum.h5')
model3 = load_model('modelSym.h5')

testFiles = os.listdir("testing/")
testFiles.sort(key=lambda x: '{0:0>8}'.format(x).lower())

for i in testFiles:
    img = image.load_img(path="testing/"+i,grayscale=True,target_size=(50,50,1))
    img = image.img_to_array(img)
    test_img = img.reshape((1,2500))
    img_class = model.predict_classes(test_img)
    prediction = img_class[0]
    classname = img_class[0]
    print("Class: ",types[classname])
    img = img.reshape((50,50))
    plt.imshow(img)
    plt.title(types[classname])
    plt.show()