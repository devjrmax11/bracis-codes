function name = segFrac(name)
I = rgb2gray(imread(name));
[Iout,intensity,fitness,time,fitnessRT,fitnessGT,fitnessBT]=segmentation(I,2,'fodpso');
IoutT = imbinarize(Iout);
%IoutT  = imcomplement(IoutT);
imwrite(IoutT, name);
save dataResults IoutT
