#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 10:58:23 2018

@author: devjrmax
"""

from keras.models import load_model
from keras.preprocessing import image
import matplotlib.pyplot as plt
import os
from natsort import natsorted
import numpy as np

def graph(formula, x_range):
    x = np.array(x_range)
    y = eval(formula)
    plt.axhline(0, color='black', lw=2)
    plt.axvline(0, color='black', lw=2)
    plt.ylim(x[0]*10,(x[-1]+1)*10)
    plt.xlim(x[0],x[-1]+1)
    plt.grid(linestyle='-', linewidth=2)
    plt.title("The polynomial is "+formula+" and the graph in the " +"\n"+ "interval of -20 and 20 is shown below")
    plt.plot(x, y, 'mo-')
    plt.show()

equation = ""
types=['number', 'symbol']
symbol =['=', '-', '(', ')', '+', '/', 'x']

# load the model we saved
model1 = load_model('modelNumSym2.h5')
model2 = load_model('modelNum3.h5')
model3 = load_model('modelSym3.h5')

testFiles = os.listdir("testing/")
testFiles = natsorted(testFiles)

fig = plt.figure()
fig.subplots_adjust(wspace=1, hspace=1)
cont = 1

for i in testFiles:
    (num, typeRec) = i.split('-')
    (typeRec, _) = typeRec.split('.')
    img = image.load_img(path="testing/"+i,grayscale=True,target_size=(28,28,1))
    img = image.img_to_array(img)
    test_img = img.reshape((1,28,28,1))
    img_class = model1.predict_classes(test_img)
    prediction = img_class[0]
    classname = img_class[0]
    #print("Class: ",types[classname])
    if(classname==0):
        img_class2 = model2.predict_classes(test_img)
        prediction2 = img_class2[0]
        classname2 = img_class2[0]
        #print("Predict: ",classname2)
        if(typeRec=='exp'):
            equation = equation+'**'+str(classname2)
        else:
            equation = equation+str(classname2)
        aux = classname2
    elif(classname==1):
        img_class3 = model3.predict_classes(test_img)
        prediction3 = img_class3[0]
        classname3 = img_class3[0]
        #print("Predict: ",symbol[classname3])
        if(typeRec=='exp'):
            equation = equation+'**'+str(symbol[classname3])
        else:
            if(len(equation)>0):
                if(equation[len(equation)-1].isnumeric and symbol[classname3]=='x'):
                    equation = equation+'*'+str(symbol[classname3])
                else:
                    equation = equation+str(symbol[classname3])
            else:
                equation = equation+str(symbol[classname3])
        aux = symbol[classname3]
    img = img.reshape((28,28))
    plt.subplot(4, 3, cont)
    plt.imshow(img)
    plt.title("Pred: "+ str(types[classname])+"(" + str(aux)+")")
    #plt.imshow(img)
    #plt.title("Class: "+str(types[classname])+". " + "Predict: " + str(aux))
    #plt.show()
    cont = cont+1
plt.show()
print(equation)
graph(equation, range(-20, 20))