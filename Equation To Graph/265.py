#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 00:48:18 2018

@author: devjrmax
"""

import math
import numpy
import pydicom
from pydicom.tag import Tag
import os
from natsort import natsorted
import re
import time
import numpngw
import glymur
from PIL import Image
import cv2
import color_cvt

bitRate = [5000, 6000, 7000, 8000, 9000, 10000, 15000, 20000, 25000, 30000, 35000, 40000]

tag = Tag(0x7fe0, 0x10)

def RGBtoHSL(r,g,b):
    h = 0
    s = 0
    l = 0

    r2 = r/255
    g2 = g/255
    b2 = b/255

    cmax = max(r2, g2, b2)
    cmin = min(r2, g2, b2)

    delta = cmax - cmin

    l = (cmax + cmin) / 2

    if (delta == 0.0):
        h = 0
        s = 0
    else:
        if (cmax == r2):
            h = 60 * (((g2 - b2) / delta) % 6)

        elif (cmax == g2):
            h = 60 * (((b2 - r2) / delta) + 2)

        elif (cmax == b2):
            h = 60 * (((r2 - g2) / delta) + 4)
        s = delta/(1.0 - abs(((2*l)-1.0)))

    return h, s, l

def HSLtoRGB(h,s,l):
    r, g, b = 0,0,0
    h = h - math.floor(h)

    if s == 0:
        r, g, b = l,l,l
    else:
        q = l * (1 + s) if l < 0.5 else l + s - l * s
        p = 2 * l - q
        r = HUEtoRGB(p, q, h + 1 / 3.0)
        g = HUEtoRGB(p, q, h)
        b = HUEtoRGB(p, q, h - 1 / 3.0)
    return r*255, g*255, b*255

def HUEtoRGB(p, q, t):
    if(t < 0):
        t += 1
    if(t > 1):
        t -= 1
    if(t < 1/6.0):
        return p + (q - p) * 6 * t
    if(t < 1/2.0):
        return q
    if(t < 2/3.0):
        return p + (q - p) * (2/3.0 - t) * 6
    return p

def listDCMFiles(path):
    lstFilesDCM = []
    for dirName, subdirList, fileList in os.walk(path):
        for filename in fileList:
            if ".dcm" in filename.lower():
                lstFilesDCM.append(os.path.join(dirName, filename))

    lstFilesDCM = natsorted(lstFilesDCM)

    return lstFilesDCM

def delta(path):

    max = -10000000
    min = 10000000

    dicoms = []

    listDicoms = listDCMFiles(path)

    for i in range(len(listDicoms)):

        ds = pydicom.read_file(listDicoms[i])

        arrayImage = ds.pixel_array

        if max < numpy.amax(arrayImage):
            max = numpy.amax(arrayImage)

        if min > numpy.amin(arrayImage):
            min = numpy.amin(arrayImage)

    delta = max - min

    return delta

def pseudocolor(arrayImage, lines, columns, delta):
    coloredDicom = numpy.zeros((lines, columns, 3), numpy.uint8)
    for i in range(lines):
        for j in range(columns):

            cor = arrayImage[i][j] / delta

            corx2 = cor * 2

            if (corx2 <= 1):
                sqrtcorx2 = math.sqrt(corx2)
            else:
                sqrtcorx2 = 2 - math.sqrt(2 - corx2)

            sqrtcorx2 = sqrtcorx2 / 2

            r, g, b = HSLtoRGB(sqrtcorx2 * math.log2(delta) / 2, 1, cor)

            coloredDicom[i][j][0], coloredDicom[i][j][1], coloredDicom[i][j][2] = round(r), round(g), round(b)

    return coloredDicom

def decompress(r, g, b, delta):

    h, s, l = color_cvt.RGBtoHSL(r, g, b)

    pixel = l * delta

    return pixel

def createOutput(path, delta):
    path = re.sub('/', '', path)
    ts = time.time()
    output = path + "-" + str(delta) + "-" + str(int(ts))
    os.makedirs(output)
    return output

def extractTag(dicomFile, path, name):
    if not os.path.exists(str(path)+"/tags"):
        os.makedirs(str(path)+"/tags")
    tag = Tag(0x7fe0, 0x10)
    file = pydicom.read_file(dicomFile)
    del file[tag]
    file.save_as(path + "/tags/" + str(name) +".dcm")

def savePng(image, path, name):
    if not os.path.exists(str(path)+"/png"):
        os.makedirs(str(path)+"/png")
    numpngw.write_png(path+"/png"+"/"+name+".png", image)

def saveJ2k(image, path, name):
    if not os.path.exists(str(path)+"/jp2"):
        os.makedirs(str(path)+"/jp2")
    glymur.Jp2k(path+"/jp2"+"/"+name+".jp2", image)

def saveJpeg(image, path, name):
    if not os.path.exists(str(path)+"/jpeg"):
        os.makedirs(str(path)+"/jpeg")
    im = Image.fromarray(image)
    im.save(path+"/jpeg"+"/"+name+".jpeg")

def saveVideo(path):
    if not os.path.exists(str(path)+"/Video"):
        os.makedirs(str(path)+"/Video")
        os.makedirs(str(path) + "/Video/jpeg")
        os.makedirs(str(path) + "/Video/png")
        os.makedirs(str(path) + "/Video/jp2")

    os.system("ffmpeg -framerate 30 -i "+path+"/png/%01d.png -codec libx265 "+path+"/Video/png/pseudocolor-png.mkv")
    os.system("ffmpeg -framerate 30 -i "+path+"/jpeg/%01d.jpeg -codec libx265 "+path+"/Video/jpeg/pseudocolor-jpeg.mkv")
    #os.system("ffmpeg -framerate 30 -i "+path+"/jp2/%01d.jp2 -codec copy " + path + "/Video/jp2/pseudocolor-jp2.mkv")

    for bit in bitRate:
        os.system("ffmpeg -i " + path + "/Video/png/pseudocolor-png.mkv -vcodec h264 -b:v "+str(bit)+ "k "+ path + "/Video/png/pseudocolor-"+str(bit)+"k.mkv")

    for bit in bitRate:
        os.system("ffmpeg -i " + path + "/Video/jpeg/pseudocolor-jpeg.mkv -vcodec h264 -b:v "+str(bit)+ "k "+ path + "/Video/jpeg/pseudocolor-"+str(bit)+"k.mkv")

    #for bit in bitRate:
       # os.system("ffmpeg -i " + path + "/Video/jp2/pseudocolor-jp2.mkv -vcodec h264 -b:v "+str(bit)+ "k "+ path + "/Video/jp2/pseudocolor-"+str(bit)+"k.mkv")

def compressDir(path):
    print("Compressing the Dicoms in folder " + path)
    deltaValue = delta(path)

    print("Delta pixel value = " + str(deltaValue))

    outputPath = createOutput(path, deltaValue)

    print("Create the output folder = " + outputPath)

    listDicoms = listDCMFiles(path)

    numImages = len(listDCMFiles(path))

    print("Creating pseudocolor images")

    for i in range(numImages):
        ds = pydicom.read_file(listDicoms[i])

        print(listDicoms[i])

        extractTag(listDicoms[i], outputPath, str(i+1))

        imageColored = pseudocolor(ds.pixel_array, ds.Rows, ds.Columns, deltaValue)

        savePng(imageColored, outputPath, str(i + 1))
        #saveJ2k(imageColored, outputPath, str(i + 1))
        saveJpeg(imageColored, outputPath, str(i + 1))

    print("Compressing in a video")
    saveVideo(outputPath)

def uncompressDir(path):
    print("Uncompressing the videos in folder " + path)
    if not os.path.exists(str(path) + "/Out"):
        os.makedirs(str(path) + "/Out")
        os.makedirs(str(path) + "/Out/jpeg")
        os.makedirs(str(path) + "/Out/png")
        #os.makedirs(str(path) + "/Out/jp2")

        for bit in bitRate:
            os.makedirs(str(path) + "/Out/jpeg/"+str(bit)+"k")
            os.system("ffmpeg -i "+path+"/Video/jpeg/pseudocolor-"+str(bit)+"k.mkv -q:v 2 "+path+"/Out/jpeg/"+str(bit)+"k/%01d.jpeg")
            os.makedirs(str(path) + "/Out/png/"+str(bit)+"k")
            os.system("ffmpeg -i "+path+"/Video/png/pseudocolor-"+str(bit)+"k.mkv -q:v 2 "+path+"/Out/png/"+str(bit)+"k/%01d.png")
            #os.makedirs(str(path) + "/Out/jp2/" + str(bit) + "k")
            #os.system("ffmpeg -i " + path + "/Video/jp2/pseudocolor-" + str(bit) + "k.mkv -qscale:v 2 " + path + "/Out/jp2/" + str(bit) + "k/%01d.jp2")

    print("Reconstructing the dicoms")
    if not os.path.exists(str(path) + "/Dicoms"):
        os.makedirs(str(path) + "/Dicoms")
        os.makedirs(str(path) + "/Dicoms/jpeg")
        os.makedirs(str(path) + "/Dicoms/png")
        #os.makedirs(str(path) + "/Dicoms/jp2")
        numFiles = (len([name for name in os.listdir(path+"/tags") if os.path.isfile(os.path.join(path+"/tags", name))]))
        src, deltaValue, timeValue = path.split("-")
        for bit in bitRate:
            print(bit)
            os.makedirs(str(path) + "/Dicoms/jpeg/"+str(bit)+"k")
            os.makedirs(str(path) + "/Dicoms/png/"+str(bit)+"k")
            #os.makedirs(str(path) + "/Dicoms/jp2/" + str(bit) + "k")
            numFiles = 50
            #for i in range(1,numFiles+1):
            for i in [1,10,20,30,40,50]:
                print(str(i)+".dcm")
                imagePng = cv2.imread(path+"/Out/png/"+str(bit)+"k/"+str(i)+".png")
                imageJpeg = cv2.imread(path + "/Out/jpeg/" + str(bit) + "k/" + str(i) + ".jpeg")
                #imageJp2 = glymur.Jp2k(path + "/Out/jp2/" + str(bit) + "k/" + str(i) + ".jp2")
                #imageJp2 = imageJp2[:]

                height, width, channels = imagePng.shape

                auxDicomPng = pydicom.read_file(path+"/tags/"+str(i)+".dcm")
                auxDicomJpeg = pydicom.read_file(path + "/tags/" + str(i) + ".dcm")
                #auxDicomJp2 = pydicom.read_file(path + "/tags/" + str(i) + ".dcm")

                auxImagePng = numpy.zeros(shape=(height, width), dtype=numpy.int16)
                auxImageJpeg = numpy.zeros(shape=(height, width), dtype=numpy.int16)
                #auxImageJp2 = numpy.zeros(shape=(height, width), dtype=numpy.int16)


                print("decompress")

                for m in range(width):
                    for n in range(height):
                        auxImagePng[m][n] = round(
                            decompress(imagePng[m][n][0], imagePng[m][n][1], imagePng[m][n][2], int(deltaValue)))
                        auxImageJpeg[m][n] = round(
                            decompress(imageJpeg[m][n][0], imageJpeg[m][n][1], imageJpeg[m][n][2], int(deltaValue)))
                        #auxImageJp2[m][n] = round(
                            #decompress(imageJp2[m][n][0], imageJp2[m][n][1], imageJp2[m][n][2], int(deltaValue)))

                print("tag")
                auxDicomPng.add_new(tag, 'OW', auxImagePng)
                auxDicomJpeg.add_new(tag, 'OW', auxImageJpeg)
                #auxDicomJp2.add_new(tag, 'OW', auxImageJp2)

                print("save")
                auxDicomPng.save_as(path+"/Dicoms/png/"+str(bit)+"k/"+str(i)+".dcm")
                auxDicomJpeg.save_as(path+"/Dicoms/jpeg/"+str(bit)+"k/"+str(i)+".dcm")
                #auxDicomJp2.save_as(path + "/Dicoms/j/" + str(bit) + "k/" + str(i) + ".dcm")


compressDir("data")

#uncompressDir("data-2384-1539627447")