import cv2 
import numpy as np 
import matplotlib.pyplot as plt

#import image 
image = cv2.imread('test8.jpg')
#grayscale 
gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) 

#binary 
ret,thresh = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV) 

#dilation 
kernel = np.ones((5,5), np.uint8) 
img_dilation = cv2.dilate(thresh, kernel, iterations=1) 


#find contours 
im2,ctrs, hier = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
#sort contours 
sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])

for i, ctr in enumerate(sorted_ctrs): 
    # Get bounding box 
    x, y, w, h = cv2.boundingRect(ctr) 
    
    # Getting ROI 
    roi = image[y:y+h, x:x+w] 
    # show ROI 
    #cv2.imshow('segment no:'+str(i),roi) 
    cv2.rectangle(image,(x,y),( x + w, y + h ),(0,255,0),2) 
    #cv2.waitKey(27) 
    if w > 15 and h > 15: 
        cv2.imwrite('testing/output/{}.png'.format(i), roi)

plt.imshow(image)
plt.title("mask")
plt.show()