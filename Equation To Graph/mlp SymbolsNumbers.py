import keras
from keras.layers import Dense
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
from keras.optimizers import Adam
import matplotlib.pyplot as plt
import numpy as np
import os
from tqdm import tqdm 
from skimage import color
import imageio
from sklearn.model_selection import  train_test_split
from keras.preprocessing import image

batch_size = 128
num_classes = 2
epochs = 20

# input image dimensions
img_rows, img_cols = 50, 50

features_list = []
features_label = []

training_directory = "isNumSymDataset"

classes = os.listdir(training_directory)
classes.sort(key=lambda x: '{0:0>8}'.format(x).lower())

numClasses = len(classes)

for i in range(numClasses):
    directoryClass = training_directory+"/"+str(i)
    for filename in tqdm(os.listdir(directoryClass)):
        if (filename.endswith('.jpg')):
            fileFull = directoryClass+"/"+filename
            training_digit_image = imageio.imread(fileFull)
            training_digit_image = color.rgb2gray(training_digit_image)
            features_list.append(training_digit_image)
            features_label.append(i)

features  = np.array(features_list, 'float64')

x_train, x_test, y_train, y_test = train_test_split(features, features_label)

x_train = x_train.astype('float32') / 255
x_test = x_test.astype('float32') / 255
print(x_train.shape)
print(x_test.shape)

x_train = x_train.reshape(200318,2500)
x_test = x_test.reshape(66773,2500)
y_train = keras.utils.to_categorical(y_train,num_classes)
y_test = keras.utils.to_categorical(y_test,num_classes)

model = Sequential()

model.add(Dense(units=32,activation="relu",input_shape=(2500,)))
model.add(Dense(units=64,activation="relu"))
model.add(Dense(units=128,activation="relu"))
model.add(Dense(units=64,activation="relu"))
model.add(Dense(units=32,activation="relu"))
model.add(Dense(units=2,activation="softmax"))

model.compile(optimizer=Adam(lr=.0001),loss="categorical_crossentropy",metrics=["accuracy"])
model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(x_test, y_test))
accuracy = model.evaluate(x=x_test,y=y_test,batch_size=batch_size)
print('Test loss:', accuracy[0])
print('Test accuracy:', accuracy[1])

testFiles = os.listdir("testing/")

for i in testFiles:
    img = image.load_img(path="testing/"+i,grayscale=True,target_size=(50,50,1))
    img = image.img_to_array(img)
    test_img = img.reshape((1,2500))
    img_class = model.predict_classes(test_img)
    prediction = img_class[0]
    classname = img_class[0]
    print("Class: ",classname)
    img = img.reshape((50,50))
    plt.imshow(img)
    plt.title(classname)
    plt.show()